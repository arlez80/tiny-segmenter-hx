package;

import tinyseg.TinySegmenter;

/**
 * ...
 * @author あるる（きのもと 結衣）
 */
class Main 
{
	
	static function main() 
	{
		var ts = new TinySegmenter( );
		var a = ts.segment( "吾輩は猫である。名前はまだ無い。どこで生れたかとんと見当がつかぬ。何でも薄暗いじめじめした所でニャーニャー泣いていた事だけは記憶している。吾輩はここで始めて人間というものを見た。" );

		trace( a.length );
		for ( t in a ) trace( t );
	}
	
}
