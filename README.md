# TinySegmenter Haxe移植

[TinySegmenter](http://chasen.org/~taku/software/TinySegmenter/)のHaxe移植です。

## Usage

```
#!haxe

var ts = new TinySegmenter( );

var words = ts.segment( "吾輩は猫である。名前はまだ無い。" );
trace( words );
```

modelやbiasを差し替えることができます。

## License

元のJavaScript版のLicenseテキストをご覧ください

[http://chasen.org/~taku/software/TinySegmenter/LICENCE.txt](http://chasen.org/~taku/software/TinySegmenter/LICENCE.txt)